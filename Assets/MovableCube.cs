﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class MovableCube : NetworkBehaviour {
    Vector3 pos;
    void Start() {
        pos = transform.position;
    }

    // Update is called once per frame
    void Update() {
        var newPos = transform.position;
        if (newPos == pos) return;

        var id = GetComponent<NetworkIdentity>().netId;

        PlayerData.localPlayer.CmdUpdatePosition(id, newPos);

        pos = newPos;
    }

    void OnGUI() {
        GUILayout.Label("" + GetComponent<NetworkIdentity>().netId);
    }
}
