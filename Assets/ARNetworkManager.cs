﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class ARNetworkManager : NetworkManager {
    public GameObject cube;
    public bool isClient;
    public override void OnStartServer() {
        var cubeInst = Instantiate(cube);
        NetworkServer.Spawn(cubeInst);
    }
}
