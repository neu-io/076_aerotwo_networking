﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;


public class PlayerData : NetworkBehaviour {

    public static PlayerData localPlayer;
    void Start() {
        if (isLocalPlayer) {
            localPlayer = this;
        }
    }

    [Command]
    public void CmdUpdatePosition(uint id, Vector3 pos) {
        Debug.Log("Updating position");
        var obj = NetworkIdentity.spawned[id];
        obj.transform.position = pos;
    }

}
